# Annotations

This document describes the annotations that are used within the *HPC Container Conformance* project. 
The goal of the annotations is to provide a common way to describe the features of a container image. The annotations are used to generate the conformance reports.

## Hardware Annotation

Please check the [Hardware Annotation](hw_annotations.md) document for more details.

## MPI Annotation

Please check the [MPI Annotation](mpi_annotations.md) document for more details.
