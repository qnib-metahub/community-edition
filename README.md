## MetaHub Registry

![MetaHub Logo](/img/metahub-logo.png)

MetaHub is a registry that takes the context of a user into account to deliver the best suited container image.

## Community Edition **BETA**

GitOps driven set of manifest collections and profiles to drive the **BETA** version of *MetaHub Community Edition*. The goal is to define collection of commonly used and public container images, for everyone to use.

## BETA Version

The current version used is a **BETA** version to refine the definitions and structures used within the *MetaHub Community Edition*.

**Be aware** that the current version is not stable and may change at any time. 
